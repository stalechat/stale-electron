
import c from '../utils/constants'


const userJoinedRoomSuccess = req => ({ type: c.USER_JOINED_ROOM_SUCCESS, req })
const userLeftRoomSuccess   = req => ({ type: c.USER_LEFT_ROOM_SUCCESS, req })


const userJoinedRoom = req => dispatch => dispatch(userJoinedRoomSuccess(req))
const userLeftRoom   = req => dispatch => dispatch(userLeftRoomSuccess(req))

const resetState = () => {
  return function(dispatch) {
    return dispatch(resetStateSuccess())
  }
}

const resetStateSuccess = (req) => {
  return {type: c.RESET_STATE, req}
}

module.exports = {
  userJoinedRoomSuccess
, userLeftRoomSuccess
, userJoinedRoom
, userLeftRoom
, resetState
, resetStateSuccess
}
