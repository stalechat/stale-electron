import c          from '../utils/constants'
import messageApi from '../api/messageApi'
import Helpers    from '../utils/helpers'


const createMessageSuccess = (req) => {
  return {type : c.CREATE_MESSAGE_SUCCESS, req}
}


const receivedMessageSuccess = (req) => {
  return {type: c.RECEIVED_MESSAGE_SUCCESS, req};
}


const createMessage = (socket, req) => dispatch =>
  messageApi.createMessage(socket, req)


const receivedMessage = (req) => {
  Helpers.newMessageSound()

  return function(dispatch) {
    return dispatch(receivedMessageSuccess(req))
  }
}


module.exports = {
  createMessageSuccess,
  receivedMessageSuccess,
  createMessage,
  receivedMessage
}
