import storage  from 'electron-json-storage'
import * as R   from 'ramda'

import c        from '../utils/constants'


// storage.clear(function(error) {
//   if (error) throw error;
// })

const setCoverUrl = (cover_url) => {
  return function(dispatch) {
    return storage.set('cover_url', cover_url, function(error) {
      if (error) throw error
      return dispatch({type: c.SET_COVER_URL, req: cover_url })
    })
  }
}


const setCoverVisibility = (cover_visible) => {
  console.log(cover_visible)
  return function(dispatch) {
    return storage.set('cover_visible', cover_visible, function(error) {
      if (error) throw error
      return dispatch({type: c.SET_COVER_VISIBILITY, req: cover_visible })
    })
  }
}


const setColorTheme = (color_theme) => {
  return function(dispatch) {
    return storage.set('color_theme', color_theme, function(error) {
      if (error) throw error
      return dispatch({type: c.SET_COLOR_THEME, req: color_theme })
    })
  }
}


const setSound = (sound) => {
  return function(dispatch) {
    return storage.set('sound', sound, function(error) {
      if (error) throw error
      return dispatch({type: c.SET_SOUND, req: sound })
    })
  }
}


const grabStorageSettings = () => {
  return function(dispatch) {
    return storage.getMany([ 'cover_url', 'color_theme', 'cover_visible', 'sound' ], function(error, req) {
      if (error) throw error;
      console.log(req);
      if (R.compose(R.not, R.isEmpty)(req.cover_url)) {
        dispatch({type: c.SET_COVER_URL, req: req.cover_url})
      }
      if (R.compose(R.not, R.isEmpty)(req.color_theme)) {
        dispatch({type: c.SET_COLOR_THEME, req: req.color_theme})
      }
      if (R.compose(R.not, R.isEmpty)(req.cover_visible)) {
        dispatch({type: c.SET_COVER_VISIBILITY, req: req.cover_visible})
      }
      if (R.compose(R.not, R.isEmpty)(req.sound)) {
        dispatch({type: c.SET_SOUND, req: req.sound})
      }
    })
  }
}


module.exports = {
  setCoverUrl,
  setColorTheme,
  setSound,
  setCoverVisibility,
  grabStorageSettings
}
