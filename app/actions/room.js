import { hashHistory }  from 'react-router'
import c        from '../utils/constants'
import roomApi  from '../api/roomApi'

const goToRoom = (req) => {
  if (!req.validation) {
    const path = "/chatroom/" + req.room.name + req.room.password
    hashHistory.push(path)
  }
}

const upsertRoom = (username, room_name, password) => {
  return function(dispatch) {

    return roomApi.upsertRoom(username, room_name, password).then(req => {
      dispatch(saveUser(req))
      goToRoom(req)
    }).catch(error => {
      throw(error)
    })

  }
}

const getRoomInfo = (room_id) => {  
  return function(dispatch) {

    return roomApi.getRoomDetails(room_id).then(req => {
      dispatch(saveRoomInfo(req))
    }).catch(error => {
      throw(error)
    })

  }
}

const resetState = () => {
  return function(dispatch) {
    return dispatch(resetStateSuccess())
  }
}

const resetStateSuccess = (req) => {
  return {type: c.RESET_STATE, req}
}

const saveRoomInfo = (req) => {
  return {type: c.SAVE_ROOM_INFO, req};
}

const saveUser = (req) => {
  return {type: c.SAVE_USER_SUCCESS, req};
}

const upsertRoomSuccess = (req) => {
  return {type: c.UPSERT_ROOM_SUCCESS, req};
}


module.exports = {
  upsertRoom,
  getRoomInfo,
  resetState,
  resetStateSuccess,
  saveRoomInfo,
  saveUser,
  upsertRoomSuccess
}
