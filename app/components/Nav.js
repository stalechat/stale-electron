import React, { PropTypes } from 'react'
import * as R               from 'ramda'
import { hashHistory }        from 'react-router'

import IconButton from './common/IconButton'


class Nav extends React.Component {

  isSelected(location) {
    return hashHistory.getCurrentLocation().pathname === location
  }

  render() {
    return (
      <section className={"nav " + (this.props.open ? "open" : "")}>

        <IconButton
          className="to-home-button"
          primaryIcon="icon-home"
          buttonText="home"
          selected={this.isSelected("/")}
          onClick={this.props.toHome}/>

        <IconButton
          className="to-chat-button"
          primaryIcon="icon-chat"
          buttonText="chat"
          selected={this.isSelected("/chatroom/:chatRoomId")}
          disabled={R.isEmpty(this.props.room)}
          onClick={this.props.toChat}/>

        <IconButton
          className="to-settings-button"
          primaryIcon="icon-cog"
          buttonText="settings"
          selected={this.isSelected("/settings")}
          onClick={this.props.toSettings}/>

      </section>
    )
  }
}

export default Nav
