import React, { PropTypes } from 'react'
import moment from 'moment'

class ChatMessages extends React.Component {

  render() {
    const user = this.props.user

    const listMessages = this.props.messages.map((msg) =>
      <section 
        key={msg.id}
        className={"message-item "+(user.id == msg.user_id ? "users" : "")}>
        <article className="message-info">

          <div className="glowing-ball">
            <i className={(user.id == msg.user_id ? "icon-vinyl" : "icon-circle")}></i>
          </div>

          <div className="message-author"> 
            <p> {msg.username} </p>
            <p> {moment(msg.created_timestamp).format("h:mm a")} </p>
          </div>

        </article>

        <article className="message-text">
           <p> {msg.message} </p>
        </article>
      </section>
    )

    return ( 
      <section className="chat-messages">
        <section className="message-list" id="message-list">
          {listMessages}
        </section>
      </section> 
    ); 
  } 
} 

ChatMessages.propTypes = {
  messages: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired
}

export default ChatMessages
