import React, { PropTypes } from 'react'
import R                    from 'ramda'

import c          from '../utils/constants'
import Helpers    from '../utils/helpers'

import TextInput  from './common/TextInput'
import IconButton from './common/IconButton'


class SettingsOptions extends React.Component {
  render() {
    return ( 
      <section className={"settings-options "+this.props.selected}>
        <section className="theme">
          <section className="title">
            <div></div>
            <h2>Theme</h2>
          </section>

          <section className="selection">
            <article> 
              <h4>Customize the look by selecting either light or dark theme.</h4>
            </article>
          
            <article className="options">
              <IconButton
                className = "light"
                onClick = {() => this.props.setColorTheme("light")} 
                selected = {this.props.color_theme == "light"}
                buttonText = "light"
                secondaryIcon = "icon-check"/>
              <IconButton
                className = "dark"
                onClick = {() => this.props.setColorTheme("dark")} 
                selected = {this.props.color_theme == "dark"}
                buttonText = "dark"
                secondaryIcon = "icon-check"/>
            </article>
          </section>
        </section>


        <section className="sound">
          <section className="title">
            <div></div>
            <h2>Sound</h2>
          </section>

          <section className="selection">
            <article> 
              <h4>Select sound notifcation for incoming messages</h4>
            </article>
          
            <article className="options">
              <IconButton
                className = "on"
                onClick = {() => this.props.setSound("on")} 
                selected = {this.props.sound == "on"}
                buttonText = "on"
                secondaryIcon = "icon-check"/>
              <IconButton
                className = "off"
                onClick = {() => this.props.setSound("off")} 
                selected = {this.props.sound == "off"}
                buttonText = "off"
                secondaryIcon = "icon-check"/>
            </article>
          </section>
        </section>

        <section className="cover">
          <section className="title">
            <div></div>
            <h2>Cover</h2>
          </section>


          <section className="selection">
            <article> 
              <h4>Toggle cover visibility</h4>
            </article>
          
            <article className="options">
              <IconButton
                className = "visible"
                onClick = {() => this.props.setCoverVisibility("visible")}
                selected = {this.props.cover_visible == "visible"}
                buttonText = "on"
                secondaryIcon = "icon-check"/>
              <IconButton
                className = "invisible"
                onClick = {() => this.props.setCoverVisibility("invisible")} 
                selected = {this.props.cover_visible == "invisible"}
                buttonText = "off"
                secondaryIcon = "icon-check"/>
            </article>
          </section>

          <section className="cover-input">
            <h4>What image do you want to use? (Povide a valid URL)</h4>
            <form>
              <section className="form-inputs"> 
                <TextInput
                  type="text"
                  id="cover_url"
                  name="cover_url"
                  value={this.props.form.cover_url}
                  onChange={this.props.onChange}/>

              </section>
              <section className="form-submit">
                <article
                  onClick={this.props.setCoverUrl}> 
                  <p> Submit </p>
                </article>
              </section>
            </form>
          </section>

          <section className="cover-preview">
            <h4>Preview: </h4>
            <img src={this.props.form.cover_url} alt="Cover Preview"/>
          </section>
        </section>

      </section>
    )
  } 
} 

SettingsOptions.propTypes = {
  form: PropTypes.object.isRequired,
  cover_visible: PropTypes.string.isRequired,
  color_theme: PropTypes.string.isRequired,
  setCoverUrl: PropTypes.func.isRequired,
  setCoverVisibility: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  setColorTheme: PropTypes.func.isRequired
}

export default SettingsOptions
