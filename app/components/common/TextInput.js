import React, { PropTypes } from 'react'
import * as R from 'ramda'


class TextInput extends React.Component {  
  render() {
    return (
      <article className="floating-input">
        <input
          type={R.propOr("text", "type", this.props)}
          required="true"
          id={this.props.id}
          name={this.props.name}
          className="form-control"
          placeholder={this.props.placeholder}
          defaultValue={this.props.value}
          onChange={this.props.onChange} />
        <label htmlFor={this.props.name}>{this.props.label}</label>
      </article>
    )
  }
}

TextInput.propTypes = {  
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
}

export default TextInput