import React, { PropTypes } from 'react'
import R                    from 'ramda'


class IconButton extends React.Component {  
  render() {
    return (
      <article 
        className={"button-icon " + 
          this.props.className + 
          (this.props.selected ? " selected " :  "") + 
          (this.props.disabled ? " disabled " : "")}
        onClick={this.props.onClick}>
        <p className={R.isNil(this.props.buttonText) ? "hidden" : ""}>
          {this.props.buttonText}
        </p>
        <i className={"primary " + this.props.primaryIcon} />
        <i className={"secondary " + this.props.secondaryIcon} />
      </article>
    )
  }
}

IconButton.propTypes = {  
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  buttonText: PropTypes.string,
  primaryIcon: PropTypes.string,
  secondaryIcon: PropTypes.string,
  selected: PropTypes.bool,
  disabled: PropTypes.bool
}

export default IconButton