import React, { PropTypes } from 'react';
import IconBtton from './common/IconButton'

class SettingsHeader extends React.Component {
  render() {
    return ( 
      <section className="settings-header"> 
        <IconBtton
          className="to-chat-button"
          icon="icon-chevron-with-circle-left"
          onClick={this.props.toChat}/>
      
        <article className="settings-title">
          <p>select and customize your</p>
          <h2>Settings</h2>
        </article>

        <article className="moving-cog">
          <i className="icon-cog"/>
        </article>
      </section>
    )
  } 
} 

SettingsHeader.propTypes = {
  toChat: PropTypes.func.isRequired
}

export default SettingsHeader