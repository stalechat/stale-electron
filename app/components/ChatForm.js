import React, { PropTypes } from 'react'
import TextInput from './common/TextInput'

class ChatForm extends React.Component {
  render() {
    return (
      <section className="chat-form"> 
        <p>{this.props.user.name}:</p>
        <form>

          <section className="form-inputs">
            <input
              type="text"
              required="true"
              id="message"
              name="message"
              value={this.props.form.message}
              onChange={this.props.onChange} /> 
          </section>

          <article 
            className="form-submit"
            onClick={this.props.onSubmit} >

            <i className="icon-paper-plane" />
            <input
              type="submit"
              disabled={this.props.disabled}
              className="btn hidden" />

          </article>

        </form>
      </section>
    )
  }
}

ChatForm.propTypes = {  
  form: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool
}

export default ChatForm