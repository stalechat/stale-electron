import React, { PropTypes } from 'react'
import * as R from 'ramda'

import TextInput from './common/TextInput'

const buttonClass = (disabled) => R.ifElse(
  R.equals(true),
  R.always("btn disabled"),
  R.always("btn")
)(disabled)

class HomeForm extends React.Component {
  render() {
    return (
      <section className="home-form"> 
        <form>

          <section className="form-inputs"> 
            <TextInput
              type="text"
              id="username"
              name="username"
              label="Username"
              value={this.props.form.username}
              onChange={this.props.onChange}/>

            <TextInput
              type="text"
              id="room_name"
              name="room_name"
              label="Room Name"
              value={this.props.form.room_name}
              onChange={this.props.onChange}/>

            <TextInput
              type="password"
              id="password"
              name="password"
              label="Password"
              value={this.props.form.password}
              onChange={this.props.onChange}/>
          </section>

          <section className="form-submit">
            <article>
              <input
                type="submit"
                disabled={this.props.disabled}
                className={buttonClass(this.props.disabled)}
                value="Continue"
                onClick={this.props.onSubmit}/>
            </article>
          </section>

        </form>
      </section>
    )
  }
}

HomeForm.propTypes = {  
  form: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool
}

export default HomeForm