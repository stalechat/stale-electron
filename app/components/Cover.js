import React, { PropTypes } from 'react';

class Cover extends React.Component {
  render() {
    return (
      <section className = {"cover-image "+(this.props.cover_visible == "invisible" ? " hidden " : "")}>
        <img src = {this.props.cover_url} alt = "Cover Image"/>
      </section>
    )
  }
}

Cover.propTypes = {
  cover_url: PropTypes.string.isRequired,
  cover_visible: PropTypes.string.isRequired
}

export default Cover
