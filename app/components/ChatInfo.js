import React, { PropTypes } from 'react'
import * as R from 'ramda'

class ChatInfo extends React.Component {
  render() {
    const listUsers = this.props.users.map((user) =>
      <article key={user.id}>
        <h4>{user.name}</h4>
      </article>
    )

    return ( 
      <section className="chat-info">
        <article className="room-name">
          <p>ROOM</p>
          <h2>{this.props.room_name}</h2>
        </article>

        <article className="users-online">
          <p>USERS ({this.props.users.length})</p>
          {listUsers}
        </article>

      </section>
    ); 
  } 
} 

ChatInfo.propTypes = {
  users: PropTypes.array.isRequired,
  room_name: PropTypes.string.isRequired
}

export default ChatInfo
