import * as R                 from 'ramda'
import React, { PropTypes }   from 'react'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import { hashHistory }        from 'react-router'
import localStorage           from 'electron-json-storage' 

import SocketLib              from '../utils/socket'
import * as messageActions    from '../actions/message'
import * as roomActions       from '../actions/room'
import * as userActions       from '../actions/user'
import * as storageActions    from '../actions/storage'

import Header from './Header'
import Cover from '../components/Cover'

class App extends React.Component {

  constructor(props, context) {
    super(props, context)
  }

  componentDidMount() {
  
    localStorage.getAll(function(error, data) {
      if (error) throw error;
      console.log(data);
    })

    this.props.storageActions.grabStorageSettings()

    SocketLib.onNewMessage(this.props.messageActions.receivedMessage)
    SocketLib.onUserJoinedRoom(this.props.userActions.userJoinedRoom)
    SocketLib.onUserLeftRoom(this.props.userActions.userLeftRoom)
  }

  render() {
    return (
      <div className={"app " + this.props.color_theme}>
        <Header />

        <section className="page">
          {this.props.children}
        </section>

        <Cover
          cover_url = {this.props.cover_url}
          cover_visible = {this.props.cover_visible} />
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element.isRequired,
  roomActions: PropTypes.object.isRequired,
  messageActions: PropTypes.object.isRequired,
  storageActions: PropTypes.object.isRequired
}


function mapStateToProps(state, ownProps) {
  return {
    user: state.user,
    users: state.room.users,
    messages: state.room.messages,
    room: state.room.details,
  
    cover_visible: state.storage.cover_visible,
    cover_url: state.storage.cover_url,
    color_theme: state.storage.color_theme
    
  }
} 

function mapDispatchToProps(dispatch) {
  return {
    roomActions: bindActionCreators(roomActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    messageActions: bindActionCreators(messageActions, dispatch),
    storageActions: bindActionCreators(storageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
