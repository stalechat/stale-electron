import React, { PropTypes }   from 'react'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import * as R                 from 'ramda'
import * as roomActions       from '../actions/room'

import HomeForm     from '../components/HomeForm'

class HomePage extends React.Component {


  constructor(props, context) {
    super(props, context)
    this.state = {
      form: {
        username: '',
        room_name: '',
        password: ''
      }
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }


  onChange(event) {
    let field = event.target.name
    let form = this.state.form
    form[field] = event.target.value
    return this.setState({form: form})
  }


  onSubmit(event) {
    event.preventDefault()
    this.props.roomActions.upsertRoom(
      this.state.form.username,
      this.state.form.room_name, 
      this.state.form.password
    )
  }

  buttonDisabled() {
    return R.any(R.isEmpty)([
      this.state.form.username,
      this.state.form.room_name,
      this.state.form.password
    ])
  }

  render() {
    return (
      <section className="page-home">

        <HomeForm
          onSubmit={this.onSubmit}
          onChange={this.onChange} 
          disabled={this.buttonDisabled()}
          form={this.state.form} />

      </section>
    )
  }
}

HomePage.propTypes = {
  roomActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {}
} 

function mapDispatchToProps(dispatch) {
  return {
    roomActions: bindActionCreators(roomActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)