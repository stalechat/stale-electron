/* eslint-disable */

import React, { PropTypes }     from 'react'
import { connect }              from 'react-redux'
import { hashHistory }          from 'react-router'
import { bindActionCreators }   from 'redux'
import * as R                   from 'ramda'
import * as roomActions         from '../actions/room'
import * as messageActions      from '../actions/message'
import * as userActions         from '../actions/user'

import SocketLib              from '../utils/socket'
import Helpers                from '../utils/helpers'

import ChatMessages           from '../components/ChatMessages'
import ChatInfo               from '../components/ChatInfo'
import ChatForm               from '../components/ChatForm'


class ChatPage extends React.Component {


  constructor(props, context) {
    super(props, context)
    this.state = {
      form: {
        message: ''
      }
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }


  componentWillMount() {
    this.props.roomActions.getRoomInfo(R.path(['user', 'room_id'], this.props))
  }


  componentDidMount() {
    SocketLib.joinRoom({
      user_id: this.props.user.id,
      room_id: this.props.user.room_id
    })
    Helpers.scrollDown()
  }


  onChange(event) {
    let field = event.target.name
    let form = this.state.form
    form[field] = event.target.value
    return this.setState({form: form})
  }


  onSubmit(event) {
    event.preventDefault()

    this.props.messageActions.createMessage(SocketLib.socket, {
      user_id : R.path(['user', 'id'], this.props),
      room_id : R.path(['user', 'room_id'], this.props),
      message : R.path(['form', 'message'], this.state)
    })

    this.clearFormInput()
  }

  clearFormInput() {
    let form = this.state.form
    form.message = ''
    return this.setState({form: form})
  }

  render() {
    return (
      <section className="page-chat">
        <ChatInfo
          users={R.pathOr([],['users'], this.props)}
          room_name={R.path(['room', 'name'], this.props)}/>

        <section className="chat-content">
          <ChatMessages
            user={this.props.user}
            messages={R.propOr([],['messages'], this.props)} />

          <ChatForm
            onSubmit={this.onSubmit}
            onChange={this.onChange}
            user={this.props.user}
            form={this.state.form} />
        </section>
      </section>
    )
  }
}


ChatPage.propTypes = {
  messageActions: PropTypes.object.isRequired,
  roomActions: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  users: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired,
  room: PropTypes.object.isRequired
}


function mapStateToProps(state, ownProps) {
  return {
    user: state.user,
    users: state.room.users,
    messages: state.room.messages,
    room: state.room.details
  }
} 


function mapDispatchToProps(dispatch) {
  return {
    messageActions: bindActionCreators(messageActions, dispatch),
    roomActions: bindActionCreators(roomActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatPage)