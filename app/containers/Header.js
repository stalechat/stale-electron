import React, { PropTypes }   from 'react'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import { hashHistory }        from 'react-router'
import * as R                 from 'ramda'

import ElectronLib            from '../utils/electron'
import SocketLib              from '../utils/socket'
import * as roomActions         from '../actions/room'
import * as messageActions      from '../actions/message'
import * as userActions         from '../actions/user'
import * as storageActions    from '../actions/storage'

import IconButton from '../components/common/IconButton'
import Nav from '../components/Nav'

class Header extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      open: false
    }
    this.toHome = this.toHome.bind(this)
    this.toChat = this.toChat.bind(this)
    this.toSettings = this.toSettings.bind(this)
    this.toggleMenu = this.toggleMenu.bind(this)
  }

  toggleMenu() {
    return this.setState({open: !this.state.open})
  }

  toHome(event) {
    event.preventDefault()

    const { id: user_id, room_id } = this.props.user
    if (room_id) {
      SocketLib.leaveRoom({user_id, room_id})
    }

    this.props.roomActions.resetState()
    this.props.userActions.resetState()

    return hashHistory.push('/')
  }

  toChat(event) {
    event.preventDefault()

    const path = "/chatroom/" + this.props.room.name + this.props.room.password
    return hashHistory.push(path)
  }

  toSettings(event) {
    event.preventDefault()

    return hashHistory.push('/settings')
  }

  render() {
    return (
      <section className="header">

        <Nav
          open={this.state.open}
          room = {this.props.room}
          toSettings = {this.toSettings}
          toChat = {this.toChat}
          toHome = {this.toHome}/>

        <section className="frame-nav">
          <article
            onClick={this.toggleMenu}
            className="menu-toggle">
            <i className="icon-dots-three-vertical"></i>
            <p>menu</p>
          </article>

          <article className="title">
            <h1>stale</h1>
          </article>

          <section className={"frame-options " + (process.platform != "win32" ? "hidden" : "")}>
            <IconButton
              className="min-button"
              primaryIcon="icon-minus"
              onClick={ElectronLib.minimizeWindow}/>
            <IconButton
              className="max-button"
              primaryIcon="icon-resize-full-screen"
              onClick={ElectronLib.maximizeWindow}/>
            <IconButton
              className="close-button"
              primaryIcon="icon-cross"
              onClick={ElectronLib.closeWindow}/>
          </section>
        </section>

      </section>
    )
  }
}

Header.propTypes = {
  roomActions: PropTypes.object.isRequired,
  messageActions: PropTypes.object.isRequired,
  storageActions: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user,
    users: state.room.users,
    messages: state.room.messages,
    room: state.room.details
  }
}

function mapDispatchToProps(dispatch) {
  return {
    roomActions: bindActionCreators(roomActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    messageActions: bindActionCreators(messageActions, dispatch),
    storageActions: bindActionCreators(storageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)