import React, { PropTypes }   from 'react';
import { connect }            from 'react-redux'
import { hashHistory }        from 'react-router'
import { bindActionCreators } from 'redux'
import * as R                 from 'ramda'

import * as roomActions         from '../actions/room'
import * as messageActions      from '../actions/message'
import * as userActions         from '../actions/user'
import * as storageActions      from '../actions/storage'
import c                        from '../utils/constants'

import SettingsOptions  from '../components/SettingsOptions'


class SettingsPage extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      form: {
        cover_url: this.props.cover_url
      }
    }
    this.onChange = this.onChange.bind(this)
    this.setCoverUrl = this.setCoverUrl.bind(this)
    this.setCoverVisibility = this.setCoverVisibility.bind(this)
    this.setColorTheme = this.setColorTheme.bind(this)
    this.setSound = this.setSound.bind(this)
  }

  onChange(event) {
    let field = event.target.name
    let form = this.state.form
    form[field] = event.target.value
    return this.setState({form: form})
  }

  setCoverUrl() {
    this.props.storageActions.setCoverUrl(this.state.form.cover_url)
  }

  setCoverVisibility(visible) {
    this.props.storageActions.setCoverVisibility(visible)
  }

  setColorTheme(color_theme) {
    console.log(color_theme, this.props.color_theme)
    this.props.storageActions.setColorTheme(color_theme)
  }

  setSound(sound) {
    console.log(sound)
    this.props.storageActions.setSound(sound)
  }

  render() {
    return (
      <section className="page-settings">

        <SettingsOptions
          cover_visible={this.props.cover_visible}
          setCoverVisibility={this.setCoverVisibility}
          form={this.state.form}
          cover_url={this.props.cover_url}
          setCoverUrl={this.setCoverUrl}
          onChange={this.onChange}
          setSound={this.setSound}
          sound={this.props.sound}
          color_theme={this.props.color_theme}
          setColorTheme={this.setColorTheme} />

      </section>
    )
  }
}

SettingsPage.propTypes = {
  storageActions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  cover_url: PropTypes.string.isRequired,
  cover_visible: PropTypes.string.isRequired,
  color_theme: PropTypes.string.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user,
    cover_visible: state.storage.cover_visible,
    cover_url: state.storage.cover_url,
    color_theme: state.storage.color_theme,
    sound: state.storage.sound
  }
} 

function mapDispatchToProps(dispatch) {
  return {
    messageActions: bindActionCreators(messageActions, dispatch),
    roomActions: bindActionCreators(roomActions, dispatch),
    storageActions: bindActionCreators(storageActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage)