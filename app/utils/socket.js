import IO           from 'socket.io-client'
import serverConfig from '../server'

const server = serverConfig()

const socket = IO(`${server}`, {
  reconnection         : true
, reconnectionDelay    : 1000
, reconnectionDelayMax : 5000
, reconnectionAttempts : Infinity
})


const socketEmitterFactory = event_name => action => {
  socket.emit(event_name, action)
}


const socketListenerFactory = event_name => action => {
  socket.on(event_name, action)
}



module.exports = {
  socket

, onNewMessage     : socketListenerFactory('new_message')
, onUserJoinedRoom : socketListenerFactory('user_joined')
, onUserLeftRoom   : socketListenerFactory('user_left')
, joinRoom         : socketEmitterFactory('join')
, leaveRoom        : socketEmitterFactory('leave')
}
