import R from 'ramda'

const defaultStyles = 
  `background-color: green; 
  color: white; 
  font-weight: bolder; 
  padding: 5px`

const styledLog = function(type, message, styles = defaultStyles) {
  console.log(
    `%c${type}%c`, 
    styles,
     "",
     message
  )
}

const newMessageSound = () => {
  const noise = new Audio('./sounds/your-turn.mp3')
  noise.play()
}

const scrollDown = () => {
  const e = document.getElementById("message-list")
  e.scrollTop = e.scrollHeight
}

const notEmpty = R.compose(R.not, R.isEmpty)

const notNil = R.compose(R.not, R.isNil)

export default {
  styledLog: styledLog,
  newMessageSound: newMessageSound,
  scrollDown: scrollDown,
  notEmpty: notEmpty,
  notNil: notNil
}
