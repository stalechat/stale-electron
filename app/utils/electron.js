const { remote } = require('electron');
const window = remote.getCurrentWindow();

const closeWindow = () => {
  window.close()
}

const minimizeWindow = () => {
  window.minimize()
}

const maximizeWindow = () => {
  if (!window.isMaximized()) {
      window.maximize();          
  } else {
      window.unmaximize();
  }
}


export default {
  closeWindow: closeWindow
, minimizeWindow: minimizeWindow
, maximizeWindow: maximizeWindow
}
