import Cryptr from 'cryptr'
import * as C from './constants'

const cryptr = new Cryptr(C.SECRET)

module.exports = { e: cryptr.encrypt, d: cryptr.decrypt }