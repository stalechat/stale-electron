import * as R from 'ramda'
import serverConfig from '../server'

const server = serverConfig()

class roomApi {


  static upsertRoom(username, room_name, password) {

    const request = new Request(`${server}/api/room`, {
      method: 'POST', 
      headers: R.merge({'Content-Type': 'application/json'}, {}),
      body: JSON.stringify({
        username: username,
        room_name: room_name,
        password: password
      })
    })

    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      return error
    })
  }


  static getRoomDetails(room_id) {

    const request = new Request(`${server}/api/room/${room_id}`, {
      method: 'GET', 
      headers: R.merge({'Content-Type': 'application/json'}, {}),
      body: {}
    })

    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      return error
    })
  }


}

export default roomApi