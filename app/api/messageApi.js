import * as R from 'ramda'
import * as E from '../utils/encrypt.js'

class messageApi {


  static createMessage(socket, req) {

    return socket.emit('send_message', {
      room_id : req.room_id,
      user_id : req.user_id,
      msg     : E.e(req.message)
    })
  }

}

export default messageApi
