const HOST = process.env.HOST || 'localhost'
const PORT_API = process.env.PORT_API || 3000

const LOCAL_URL = `http://${HOST}:${PORT_API}`

const HEROKU_URL = 'https://damp-beyond-88335.herokuapp.com'

const serverConfig = function() {
  if (process.env.NODE_ENV == 'production') {
    return HEROKU_URL
  } else {
    return LOCAL_URL
  }
}


export default serverConfig
