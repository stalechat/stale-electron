import c      from '../utils/constants'
import * as R from 'ramda'

import { hashHistory } from 'react-router'
import initialState from './initialState'

import { styledLog } from '../utils/helpers'

export default function userReducer(state = initialState.user, action) {
  styledLog('userReducer', action)

  switch (action.type) {

  case c.SAVE_USER_SUCCESS:
    return R.merge(state, action.req.user)

  case c.RESET_STATE:
    return initialState.user

  default:
    return state
  }
}