import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import user     from './user'
import room     from './room'
import storage  from './storage'

const rootReducer = combineReducers({
  user,
  room,
  storage,
  routing
})

export default rootReducer
