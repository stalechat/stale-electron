export default {  

  user: {},

  room: {
    details: {},
    users: [],
    messages: []
  },

  storage: {
    cover_url: "https://static.pexels.com/photos/249798/pexels-photo-249798.png",
    cover_visible: "hidden",
    color_theme: "light",
    sound: "on"
  }
}