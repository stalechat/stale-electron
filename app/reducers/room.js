import c                from '../utils/constants'
import * as R           from 'ramda'
import * as E           from '../utils/encrypt'
import { hashHistory }  from 'react-router'
import initialState     from './initialState'
import { styledLog, scrollDown }    from '../utils/helpers'


const dc = R.evolve({message:E.d})

const addUser = R.compose(R.uniqBy(R.prop('id')), R.append)

const removeUser = (user, users) => R.reject(R.eqProps('id', user), users)


export default function roomReducer(state = initialState.room, action) {
  styledLog('roomReducer', action)

  switch (action.type) {

  case c.UPSERT_ROOM_SUCCESS:
    if (!action.validation) {
    } else {
    }
  
  case c.SAVE_ROOM_INFO:
    if (!action.validation) {
      return R.merge(state, {
        details: action.req.room,
        users: action.req.users,
        messages: R.map(dc, action.req.messages)
      })
    } else {
    }

  case c.CREATE_MESSAGE_SUCCESS:
    if (!action.validation) {
    } else {
    }

  case c.RECEIVED_MESSAGE_SUCCESS:
    window.setTimeout(() => {
      scrollDown()
    }, 10)

    return R.merge(state, {
      messages : R.append(dc(action.req), state.messages)
    })

  case c.USER_JOINED_ROOM_SUCCESS:
    return R.merge(state, {
      users: addUser(action.req, state.users)
    })

  case c.USER_LEFT_ROOM_SUCCESS:
    return R.merge(state, {
      users: removeUser(action.req, state.users)
    })

  case c.RESET_STATE:
    return initialState.room

  default:
    return state
  }
}