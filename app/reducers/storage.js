import { hashHisstory } from 'react-router'
import initialState     from './initialState'
import c                from '../utils/constants'
import * as R           from 'ramda'

import { styledLog } from '../utils/helpers'


export default function roomReducer(state = initialState.storage, action) {
  styledLog('storageReducer', action)

  switch (action.type) {

  case c.SET_COVER_URL:
    return R.merge(state, {
      cover_url: action.req
    })

  case c.SET_COVER_VISIBILITY:
    return R.merge(state, {
      cover_visible: action.req
    })


  case c.SET_COLOR_THEME:
    return R.merge(state, {
      color_theme: action.req
    })

  case c.SET_SOUND:
    return R.merge(state, {
      sound: action.req
    })


  case c.GRAB_STORAGE_SETTINGS:
    return R.merge(state, {
      cover_url: action.req.cover_url,
      color_theme: action.req.color_theme,
      cover_visible: action.req.cover_visible,
      sound: action.req.sound
    })

  default:
    return state
  }
}