import React                  from 'react';
import { Route, IndexRoute }  from 'react-router';

import App              from './containers/App';
import HomePage         from './containers/HomePage';
import SettingsPage     from './containers/SettingsPage';
import ChatPage         from './containers/ChatPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/settings" component={SettingsPage} />
    <Route path="/chatroom/:chatRoomId" component={ChatPage} />
  </Route>
);
